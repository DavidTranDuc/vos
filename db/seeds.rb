# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

#Developement data
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerA','playera',6,5,31,27,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerB','playerb',15,5,37,24,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerC','playerc',28,5,407,352,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerD','playerd',3,5,7,19,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerE','playere',1,5,3,1,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerF','playerf',27,5,103,45,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerG','playerg',7,5,57,58,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerH','playerh',11,5,87,84,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerI','playeri',24,5,207,153,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerJ','playerj',14,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerK','playerk',0,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerL','playerl',1,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerM','playerm',2,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerN','playern',3,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerO','playero',4,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerP','playerp',5,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerQ','playerq',6,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerR','playerr',7,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerS','players',8,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerT','playert',9,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerU','playeru',10,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerV','playerv',11,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerX','playerx',12,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerY','playery',13,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('PlayerZ','playerz',14,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player1','player1',15,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player2','player2',16,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player3','player3',17,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player4','player4',18,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player5','player5',19,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player6','player6',20,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player7','player7',21,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player8','player8',22,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player9','player9',23,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player10','player10',24,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player11','player11',25,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player12','player12',26,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player13','player13',27,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,concrete_league_id,region_id,win,lose,activated,password_digest,created_at,updated_at) VALUES ('Player14','player14',28,5,70,78,'true','#{Player.digest('123456')}',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")

#Deployment data
#Default events
ActiveRecord::Base.connection.execute("INSERT INTO events (name,created_at,updated_at) VALUES ('Casual playing',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")
ActiveRecord::Base.connection.execute("INSERT INTO events (name,created_at,updated_at) VALUES ('Competitive playing',timestamptz '#{Time.now}',timestamptz '#{Time.now}');")

#Default regions
ActiveRecord::Base.connection.execute("INSERT INTO regions (name, short) VALUES ('Korea','kr');")
ActiveRecord::Base.connection.execute("INSERT INTO regions (name, short) VALUES ('Japan','jp');")
ActiveRecord::Base.connection.execute("INSERT INTO regions (name, short) VALUES ('North America','na');")
ActiveRecord::Base.connection.execute("INSERT INTO regions (name, short) VALUES ('Europe West','euw');")
ActiveRecord::Base.connection.execute("INSERT INTO regions (name, short) VALUES ('Europe Nordic & East','eune');")
ActiveRecord::Base.connection.execute("INSERT INTO regions (name, short) VALUES ('Oceania','oce');")
ActiveRecord::Base.connection.execute("INSERT INTO regions (name, short) VALUES ('Brazil','br');")
ActiveRecord::Base.connection.execute("INSERT INTO regions (name, short) VALUES ('LAS','las');")
ActiveRecord::Base.connection.execute("INSERT INTO regions (name, short) VALUES ('LAN','lan');")
ActiveRecord::Base.connection.execute("INSERT INTO regions (name, short) VALUES ('Russia','ru');")
ActiveRecord::Base.connection.execute("INSERT INTO regions (name, short) VALUES ('Turkey','tr');")

#Default leagues
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Unranked',0,'unranked');")

ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Bronze V',100,'bronze_5');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Bronze IV',110,'bronze_4');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Bronze III',120,'bronze_3');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Bronze II',130,'bronze_2');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Bronze I',140,'bronze_1');")

ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Silver V',200,'silver_5');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Silver IV',210,'silver_4');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Silver III',220,'silver_3');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Silver II',230,'silver_2');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Silver I',240,'silver_1');")

ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Gold V',300,'gold_5');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Gold IV',310,'gold_4');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Gold III',320,'gold_3');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Gold II',330,'gold_2');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Gold I',340,'gold_1');")

ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Platinum V',400,'platinum_5');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Platinum IV',410,'platinum_4');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Platinum III',420,'platinum_3');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Platinum II',430,'platinum_2');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Platinum I',440,'platinum_1');")

ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Diamond V',500,'diamond_5');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Diamond IV',510,'diamond_4');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Diamond III',520,'diamond_3');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Diamond II',530,'diamond_2');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Diamond I',540,'diamond_1');")

ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Master',640,'master_1');")

ActiveRecord::Base.connection.execute("INSERT INTO concrete_leagues (division, rating, img) VALUES ('Challenger',740,'challenger_1');")

#Default roles
ActiveRecord::Base.connection.execute("INSERT INTO concrete_roles (name) VALUES ('Top');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_roles (name) VALUES ('Mid');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_roles (name) VALUES ('Jungle');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_roles (name) VALUES ('Support');")
ActiveRecord::Base.connection.execute("INSERT INTO concrete_roles (name) VALUES ('Bot');")