# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160620113539) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "concrete_leagues", force: :cascade do |t|
    t.string  "division"
    t.integer "rating"
    t.string  "img"
  end

  create_table "concrete_roles", force: :cascade do |t|
    t.string "name"
  end

  create_table "events", force: :cascade do |t|
    t.integer  "host_id"
    t.integer  "region_id"
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "hosts", force: :cascade do |t|
    t.string   "name"
    t.string   "login"
    t.string   "password_digest"
    t.string   "email"
    t.string   "activation_digest"
    t.boolean  "activated"
    t.datetime "activated_at"
    t.text     "note"
    t.datetime "created_at",        null: false
    t.datetime "updated_at",        null: false
  end

  create_table "player_is_lfgs", force: :cascade do |t|
    t.integer "player_id"
    t.integer "role_list_id"
    t.integer "concrete_league_id_min"
    t.integer "concrete_league_id_max"
    t.integer "event_id"
    t.text    "note"
  end

  create_table "player_requested_membership_in_teams", force: :cascade do |t|
    t.integer  "player_id"
    t.integer  "team_id"
    t.integer  "role_list_id"
    t.text     "note"
    t.datetime "created_at",   null: false
    t.datetime "updated_at",   null: false
  end

  create_table "player_teams", force: :cascade do |t|
    t.integer  "player_id"
    t.integer  "team_id"
    t.integer  "concrete_role_id"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
  end

  create_table "players", force: :cascade do |t|
    t.integer  "region_id"
    t.integer  "concrete_league_id"
    t.string   "name"
    t.string   "login"
    t.integer  "win"
    t.integer  "lose"
    t.string   "password_digest"
    t.string   "remember_digest"
    t.string   "activation_digest"
    t.boolean  "activated"
    t.datetime "activated_at"
    t.text     "note"
    t.datetime "created_at",         null: false
    t.datetime "updated_at",         null: false
    t.integer  "riot_summoner_id"
    t.integer  "league_points"
  end

  create_table "regions", force: :cascade do |t|
    t.string "name"
    t.string "short"
  end

  create_table "role_lists", force: :cascade do |t|
  end

  create_table "roles", force: :cascade do |t|
    t.integer "role_list_id"
    t.integer "concrete_role_id"
    t.string  "name"
  end

  create_table "team_invited_players", force: :cascade do |t|
    t.integer  "team_id"
    t.integer  "player_is_lfg_id"
    t.text     "note"
    t.datetime "created_at",       null: false
    t.datetime "updated_at",       null: false
    t.integer  "concrete_role_id"
  end

  create_table "teams", force: :cascade do |t|
    t.integer  "id_event"
    t.integer  "id_player"
    t.integer  "id_region"
    t.integer  "id_role_list"
    t.integer  "concrete_league_id_min_plays_in"
    t.integer  "concrete_league_id_max_plays_in"
    t.integer  "concrete_league_id_min_lfm"
    t.string   "concrete_league_id_max_lfm"
    t.string   "name"
    t.text     "note"
    t.datetime "created_at",                      null: false
    t.datetime "updated_at",                      null: false
  end

end
