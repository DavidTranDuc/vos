class RenameColumnRoleListIdInPlayerTeamsToConcreteRoleId < ActiveRecord::Migration
  def change
    rename_column :player_teams, :role_list_id, :concrete_role_id
  end
end
