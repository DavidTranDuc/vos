class CreateHost < ActiveRecord::Migration
  def change
    create_table :hosts do |t|
      t.string :name
      t.string :login
      t.string :password_digest
      t.string :email
      t.string :activation_digest
      t.boolean :activated
      t.datetime :activated_at
      t.text :note

      t.timestamps null: false
    end
  end
end
