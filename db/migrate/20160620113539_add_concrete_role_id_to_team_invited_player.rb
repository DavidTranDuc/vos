class AddConcreteRoleIdToTeamInvitedPlayer < ActiveRecord::Migration
  def change
    add_column :team_invited_players, :concrete_role_id, :integer
  end
end
