class CreatePlayer < ActiveRecord::Migration
  def change
    create_table :players do |t|
      t.integer :region_id
      t.integer :concrete_league_id
      t.string :name
      t.string :login
      t.integer :win
      t.integer :lose
      t.string :password_digest
      t.string :remember_digest
      t.string :activation_digest
      t.boolean :activated
      t.datetime :activated_at
      t.text :note

      t.timestamps null: false
    end
  end
end
