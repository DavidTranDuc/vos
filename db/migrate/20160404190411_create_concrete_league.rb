class CreateConcreteLeague < ActiveRecord::Migration
  def change
    create_table :concrete_leagues do |t|
      t.string :division
      t.integer :rating
      t.string :img
    end
  end
end
