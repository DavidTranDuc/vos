class CreatePlayerRequestedMembershipInTeam < ActiveRecord::Migration
  def change
    create_table :player_requested_membership_in_teams do |t|
      t.integer :player_id
      t.integer :team_id
      t.integer :role_list_id
      t.text :note

      t.timestamps null: false
    end
  end
end
