class CreateEvent < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.integer :host_id
      t.integer :region_id
      t.string :name

      t.timestamps null: false
    end
  end
end
