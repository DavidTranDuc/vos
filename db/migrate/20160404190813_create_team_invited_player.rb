class CreateTeamInvitedPlayer < ActiveRecord::Migration
  def change
    create_table :team_invited_players do |t|
      t.integer :team_id
      t.integer :player_is_lfg_id
      t.text :note

      t.timestamps null: false
    end
  end
end
