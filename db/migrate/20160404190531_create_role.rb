class CreateRole < ActiveRecord::Migration
  def change
    create_table :roles do |t|
      t.integer :role_list_id
      t.integer :concrete_role_id
      t.string :name
    end
  end
end
