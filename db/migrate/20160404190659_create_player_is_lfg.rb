class CreatePlayerIsLfg < ActiveRecord::Migration
  def change
    create_table :player_is_lfgs do |t|
      t.integer :player_id
      t.integer :role_list_id
      t.integer :concrete_league_id_min
      t.integer :concrete_league_id_max
      t.integer :event_id
      t.text :note
    end
  end
end
