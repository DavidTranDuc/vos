class AddLeaguePointsToPlayers < ActiveRecord::Migration
  def change
    add_column :players, :league_points, :int
  end
end
