class CreateTeam < ActiveRecord::Migration
  def change
    create_table :teams do |t|
      t.integer :id_event
      t.integer :id_player
      t.integer :id_region
      t.integer :id_role_list
      t.integer :concrete_league_id_min_plays_in
      t.integer :concrete_league_id_max_plays_in
      t.integer :concrete_league_id_min_lfm
      t.string :concrete_league_id_max_lfm
      t.string :name
      t.text :note

      t.timestamps null: false
    end
  end
end
