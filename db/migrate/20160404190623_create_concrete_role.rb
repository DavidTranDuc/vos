class CreateConcreteRole < ActiveRecord::Migration
  def change
    create_table :concrete_roles do |t|
      t.string :name
    end
  end
end
