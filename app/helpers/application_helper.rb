module ApplicationHelper

  def get_player_lfgs_roles(player_lfg_id)
    ConcreteRole.find_by_sql(
                    "SELECT cr.id, cr.name FROM
                    (SELECT * FROM player_is_lfgs plfg
                    WHERE id=#{player_lfg_id}
                    LIMIT 1) plfg
                    JOIN role_lists lr
                    ON lr.id = plfg.role_list_id
                    JOIN roles r
                    ON r.role_list_id = lr.id
                    JOIN concrete_roles cr
                    ON cr.id = r.concrete_role_id"
    )
  end

  def get_team_occupied_roles(team_id)
    ConcreteRole.find_by_sql(
                    "SELECT cr.id, cr.name FROM
                    (SELECT * FROM teams
                    WHERE id=#{team_id}
                    LIMIT 1) t
                    JOIN role_lists rl
                    ON rl.id = t.id_role_list
                    JOIN roles r
                    ON r.role_list_id = rl.id
                    JOIN concrete_roles cr
                    ON cr.id = r.concrete_role_id"
    )
  end

  def get_team_members(team_id)
    Player.find_by_sql(
               "SELECT p.* FROM
                (SELECT * FROM teams
                WHERE id=#{team_id}
                LIMIT 1) t
                JOIN player_teams pt
                ON pt.team_id = t.id
                JOIN players p
                ON pt.player_id = p.id"
    )
  end

  def get_available_roles(team_id)
    ConcreteRole.find_by_sql(
                    "((SELECT * FROM concrete_roles)
                    EXCEPT
                    (SELECT cr.id, cr.name FROM
                    (SELECT * FROM teams
                    WHERE id=#{team_id}
                    LIMIT 1) t
                    JOIN role_lists rl
                    ON rl.id = t.id_role_list
                    JOIN roles r
                    ON r.role_list_id = rl.id
                    JOIN concrete_roles cr
                    ON cr.id = r.concrete_role_id))
                    ORDER BY id
                    "
    )
  end

  def get_concrete_league_by_id(league_id)
    ConcreteLeague.find_by(id: league_id)
  end

  def region_by_id(region_id)
    Region.find_by(id: region_id)
  end

  def get_memberships_by_team_id(team_id)
    #PlayerTeam.find_by(team_id: team_id)
    PlayerTeam.find_by_sql("SELECT * FROM player_teams WHERE team_id = #{team_id}")
  end

  def get_concrete_role_by_id(role_id)
    ConcreteRole.find_by(id: role_id)
  end

  def get_player_by_membership_id(ms_id)
    Player.find_by_sql(
              "SELECT p.* FROM
              (SELECT * FROM player_teams
              WHERE id = #{ms_id}
              LIMIT 1) ms
              JOIN players p
              ON p.id = ms.player_id"
    )
  end

  def get_player_is_lfg_by_team_invited_player_id(id)
    PlayerIsLfg.find_by(id: id)
  end

  def get_player_by_lfg_id(id)
    Player.find_by(id: id)
  end

end
