module PlayersHelper

  def get_roles(role_list_id)
    role_list = ConcreteRole.find_by_sql(
        "SELECT cr.* FROM concrete_roles cr
        JOIN roles r ON r.concrete_role_id = cr.id
        WHERE r.role_list_id = #{role_list_id}"
    )
  end

  def teams_of_which_current_player_is_captain
    if current_player.nil?
      return nil
    else
      @teams = Team.find_by(id_player: current_player.id)
    end
  end

end
