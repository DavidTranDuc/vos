require 'json'

class StaticPagesController < ApplicationController

  def home
  end

  def statistics
    @chart_raw_data = $redis.zrange('playersByLeague', 0, -1, :with_scores => true)
    @chart_data_unsorted = []
    @chart_raw_data.each_with_index do |item, index|
      @chart_data_unsorted[index] = []
      @chart_data_unsorted[index][0] = (JSON.parse(item[0])[0]).to_i
      @chart_data_unsorted[index][1] = JSON.parse(item[0])[1]
      @chart_data_unsorted[index][2] = item[1]
    end
    @chart_data = []
    @chart_data_unsorted.each do |item|
      @chart_data[item[0] - 1] = []
      @chart_data[item[0] - 1][0] = item[1]
      @chart_data[item[0] - 1][1] = item[2]
    end
  end
end
