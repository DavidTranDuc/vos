class InvitesController < ApplicationController

  def show
    if current_player.nil?
      redirect_to login_path
      return
    end

    @curr_player = current_player

    @my_posters = PlayerIsLfg.find_by(player_id: @curr_player.id)
    @invitations_into_teams = nil
    unless @my_posters.nil?
      @invitations_into_teams = TeamInvitedPlayer.find_by_sql("SELECT * FROM team_invited_players WHERE player_is_lfg_id = #{@my_posters.id}")
    end

    #this will go into my teams management
    #@my_teams = Team.find_by(id_player: @curr_player.id)
    #@my_invitations = TeamInvitedPlayer.find_by(team_id: @my_teams)
  end

  def accept
    #raise params[:accept_data].inspect

    if current_player.nil?
      redirect_to login_path
      return
    end

    if params[:accept_data].nil?
      redirect_to invitations_path
      return
    end

    parameters = params[:accept_data]

    invitation_id = parameters[:invitation_id]
    invitation = TeamInvitedPlayer.find_by(id: invitation_id)
    if invitation.nil?
      redirect_to invitations_path
      return
    end

    invited_player = Player.find_by(id: parameters[:player_id])
    players_of_team = get_team_members(parameters[:team_id])
    team_roles = get_team_occupied_roles(parameters[:team_id])
    team = Team.find_by(id: parameters[:team_id])

    if current_player.id.to_i == invited_player.id.to_i

      @isValidMi = 1
      #Check if to be invited player is not already member of team
      players_of_team.each do |pot|
        if pot.id.to_i == invited_player.id.to_i
          @isValidMi = 0
          break
        end
      end

      @isValidTRi = 1
      #Check if role that to be invited player wants to play isn't already occupied in team roster
      team_roles.each do |tr|
        if tr.id.to_i == parameters[:concrete_role_id].to_i
          @isValidTRi = 0
        end
      end

      @isValidRei = 0
      #Check if player is from same region as is team
      if invited_player.region_id.to_i == team.id_region.to_i
        @isValidRei = 1
      end

      if (@isValidMi == 1 && @isValidTRi == 1 && @isValidTRi == 1)
        membership = PlayerTeam.new
        membership.player_id = parameters[:player_id]
        membership.team_id = parameters[:team_id]
        membership.concrete_role_id = parameters[:concrete_role_id]
        membership.created_at = Time.now
        membership.updated_at = Time.now

        team_role_list_id = team.id_role_list
        team_add_role_id = parameters[:concrete_role_id]

        new_role = Role.new
        new_role.role_list_id = team_role_list_id
        new_role.concrete_role_id = team_add_role_id

        #ActiveRecord::Base.transaction do
          ActiveRecord::Base.connection.execute(
              "DELETE FROM team_invited_players WHERE id=#{invitation_id};"
          )
          membership.save
          new_role.save
        #end

        redirect_to team
      end
    end
  end

  def destroy
    if current_player.nil?
      redirect_to login_path
      return
    end

    invitation_id = params[:format]
    invitation = TeamInvitedPlayer.find_by(id: invitation_id)
    if invitation.nil?
      redirect_to invitations_path
      return
    end
    invited_players_lfg = PlayerIsLfg.find_by(id: invitation.player_is_lfg_id)
    invited_player = Player.find_by(id: invited_players_lfg.player_id)
    if current_player.id.to_i == invited_player.id.to_i
      ActiveRecord::Base.connection.execute(
          "DELETE FROM team_invited_players WHERE id=#{invitation_id};"
      )
    end
    redirect_to invitations_path
  end

end
