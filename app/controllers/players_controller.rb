require 'net/http'
require 'json'


class PlayersController < ApplicationController
  before_action :logged_in_user, only: [:edit, :edit2, :update]

  def new
    @player = Player.new
  end

  def index
    #@players = Player.paginate(page: params[:page])
    #@player_is_lfgs = PlayerIsLfg.all
    @player_is_lfgs = PlayerIsLfg.find_by_sql("SELECT * FROM player_is_lfgs")

    if params[:search_lfg]
      parameters = params[:search_lfg]
      @minimal_league_rating = ConcreteLeague.find_by(id: parameters[:minimal_league_id]).rating
      @maximal_league_rating = ConcreteLeague.find_by(id: parameters[:maximal_league_id]).rating
      if (parameters[:top] == '1' || parameters[:jungle] == '1' || parameters[:mid] == '1' || parameters[:support] == '1' || parameters[:bot] == '1') && ( @minimal_league_rating.to_i <= @maximal_league_rating.to_i )
        conditions = []
        if parameters[:top] == '1'
          conditions << "cr.name = 'Top'"
        end
        if parameters[:jungle] == '1'
          conditions <<  "cr.name = 'Jungle'"
        end
        if parameters[:mid] == '1'
          conditions <<  "cr.name = 'Mid'"
        end
        if parameters[:support] == '1'
          conditions <<  "cr.name = 'Support'"
        end
        if parameters[:bot] == '1'
          conditions <<  "cr.name = 'Bot'"
        end
        where_str = "WHERE (#{conditions.join(" OR ")}) AND cl.rating >= #{@minimal_league_rating} AND cl.rating <= #{@maximal_league_rating}"

        sql_str = 'SELECT DISTINCT lfg.* FROM player_is_lfgs lfg
                      JOIN role_lists rl ON lfg.role_list_id=rl.id
                      JOIN roles r ON r.role_list_id=rl.id
                      JOIN concrete_roles cr ON r.concrete_role_id=cr.id
                      JOIN players p ON lfg.player_id=p.id
                      JOIN concrete_leagues cl ON p.concrete_league_id=cl.id ' + where_str

        @player_is_lfgs = PlayerIsLfg.find_by_sql(sql_str)
      end
    end
  end

  def show
    @player = Player.find_by_sql("SELECT * FROM players WHERE id=#{params[:id]} LIMIT 1")[0]
    if @player.nil?
      redirect_to players_path
    else
      session[:viewed_player_id] = @player.id

      @ar = ActiveRecord::Base.connection.execute(
          "SELECT p.id, COUNT(lfg.id) as c FROM players p
          JOIN player_is_lfgs lfg ON lfg.player_id = p.id
          WHERE p.id=#{@player.id}
          GROUP BY p.id;"
      )

      @player_is_lfgs = PlayerIsLfg.find_by_sql("SELECT * FROM player_is_lfgs p WHERE player_id=#{@player.id};")
    end
  end

  def create
    @player = Player.new(params.require(:player).permit(:name, :login, :password, :password_confirmation, :region_id))
    parameters = params[:player]
    if @player.valid?
      region = Region.find_by(id: parameters[:region_id])
      uri = URI.parse("https://#{region.short}.api.pvp.net/api/lol/#{region.short}/v1.4/summoner/by-name/#{parameters[:name]}?api_key=#{ENV['riot_api_key']}")
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE
      request = Net::HTTP::Get.new(uri.request_uri)
      response = http.request(request)
      parsed = JSON.parse(response.body)

      if(parsed["status"].nil?)
        #raise "actuall data"
        sql_in_name = ActiveRecord::Base.sanitize parameters[:name]
        sql_in_login = ActiveRecord::Base.sanitize parameters[:login]
        sql_in_region_id = ActiveRecord::Base.sanitize parameters[:region_id]
        sql_in_password_digest = Player.digest parameters[:password]
        sql_in_riot_summoner_id = parsed[parameters[:name].downcase]["id"]
        ActiveRecord::Base.connection.execute("INSERT INTO players (name,login,region_id,concrete_league_id,win,lose,password_digest,created_at,updated_at,riot_summoner_id) VALUES (#{sql_in_name},#{sql_in_login},#{sql_in_region_id},1,0,0,'#{sql_in_password_digest}',timestamptz '#{Time.now}',timestamptz '#{Time.now}',#{sql_in_riot_summoner_id});")
        pp = Player.find_by(login: parameters[:login])
        log_in pp
        #$redis.zincrby("playersByLeague",1,"[\"1\",\"Unranked\"]")
        #flash[:success] = 'Welcome'
        redirect_to pp
      else
        if(parsed["status"]["status_code"] == 404)
          @not_found_error = 1
        elsif(parsed["status"]["status_code"] == 429)
          @overload_error = 1
        else
          @unknown_error = 1
        end
        render 'new'
      end
    else
      render 'new'
    end
  end

  def edit
    @player = Player.find_by_sql("SELECT * FROM players WHERE id=#{current_player.id} LIMIT 1")[0]
  end

  def update
    @player = Player.find_by_sql("SELECT * FROM players WHERE id=#{current_player.id} LIMIT 1")[0]

    parameters = params[:player_data]

    @error_wrong_confirm = 0
    @error_wrong_pass = 0
    if @player && @player.authenticate(parameters[:password_old])
      if parameters[:password] == parameters[:password_confirmation]
        sql_in_password_digest = Player.digest parameters[:password]
        ActiveRecord::Base.connection.execute("UPDATE players SET password_digest = '#{sql_in_password_digest}', updated_at = timestamptz '#{Time.now}' WHERE id = #{@player.id};")
        redirect_to @player
        return
      else
        @error_wrong_confirm = 1
      end
    else
      @error_wrong_pass = 1
    end
    render 'edit'

    # if @player.update_attributes(player_params)
    #   redirect_to @player
    # else
    #   render 'edit'
    # end
  end

  def edit2
    @player = Player.find_by_sql("SELECT * FROM players WHERE id=#{current_player.id} LIMIT 1")[0]
  end

  def update2
    @player = Player.find_by_sql("SELECT * FROM players WHERE id=#{current_player.id} LIMIT 1")[0]
    parameters = params[:player]
    div = ConcreteLeague.find_by(id: current_player.concrete_league_id)[:division]
    member = JSON.generate([current_player.concrete_league_id.to_s, div])
    $redis.zincrby('playersByLeague',-1,member)
    ActiveRecord::Base.connection.execute("UPDATE players SET concrete_league_id = #{parameters[:concrete_league_id]}, win = #{parameters[:win]}, lose = #{parameters[:lose]}, updated_at = timestamptz '#{Time.now}' WHERE id = #{@player.id};")
    div = ConcreteLeague.find_by(id: parameters[:concrete_league_id])[:division]
    member = JSON.generate([parameters[:concrete_league_id], div])
    $redis.zincrby('playersByLeague',1,member)
    redirect_to @player
  end

  def put
    if session[:viewed_player_id].nil?
    else
      @player = Player.find_by_sql("SELECT * FROM players WHERE id=#{ session[:viewed_player_id] } LIMIT 1")[0]
      session.delete(:viewed_player_id)

      region = Region.find_by(id: @player.region_id)
      uri = URI.parse("https://#{region.short}.api.pvp.net/api/lol/#{region.short}/v2.5/league/by-summoner/#{@player.riot_summoner_id}/entry/?api_key=#{ENV['riot_api_key']}")
      http = Net::HTTP.new(uri.host, uri.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE

      request = Net::HTTP::Get.new(uri.request_uri)

      response = http.request(request)
      parsed = JSON.parse(response.body)


      if parsed["status"].nil?
        #raise "actuall data"
        #raise parsed[@player.riot_summoner_id.to_s].inspect
        player_data_all = parsed[@player.riot_summoner_id.to_s]
        player_data = nil
        player_data_all.each do |queue|
          if queue["queue"].eql? "RANKED_SOLO_5x5"
            player_data = queue
            break
          end
        end


        unless player_data.nil?
          default_entry = player_data["entries"][0]
          tier = player_data["tier"]
          division = default_entry["division"]
          league_points = default_entry["leaguePoints"]
          wins = default_entry["wins"]
          losses = default_entry["losses"]

          if tier.eql? "Challenger" || tier.eql? == "Master"
            div_name = "#{tier}"
          elsif
          div_name = "#{tier} #{division}"
          end

          div_id = ConcreteLeague.find_by_sql("SELECT * FROM concrete_leagues WHERE lower(division) LIKE '#{div_name.underscore}' LIMIT 1")[0][:id]

          # redis
          # div = ConcreteLeague.find_by(id: current_player.concrete_league_id)[:division]
          # member = JSON.generate([current_player.concrete_league_id.to_s, div])
          # $redis.zincrby('playersByLeague',-1,member)
          # div = ConcreteLeague.find_by(id: div_id)[:division]
          # member = JSON.generate([:div_id, div])
          # $redis.zincrby('playersByLeague',1,member)

          ActiveRecord::Base.connection.execute("UPDATE players SET concrete_league_id = #{div_id}, win = #{wins}, lose = #{losses}, league_points = #{league_points},updated_at = timestamptz '#{Time.now}' WHERE id = #{@player.id};")
        end
      else
        if(parsed["status"]["status_code"] == 404)
          @not_found_error = 1
        elsif(parsed["status"]["status_code"] == 429)
          @overload_error = 1
        else
          @unknown_error = 1
        end
      end
    end

    redirect_to @player
  end

  def get_roles
    player_lfg_id = params[:player_lfg_id]

    @roles = get_player_lfgs_roles(player_lfg_id)

    render layout: false
  end

  private

    def player_params
      params.require(:player).permit(:password, :password_confirmation)
    end

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

end
