class PlayerIsLfgsController < ApplicationController
  before_action :logged_in_user, only: [:create, :destroy]

  def new
    @player_is_lfgs = PlayerIsLfg.new
  end

  def create
    parameters = params[:new_lfg]
    @error = 0
    if parameters[:minimal_league_id].to_i > parameters[:maximal_league_id].to_i
      @error = 2
      render 'new'
      return
    elsif parameters[:top] == '1' || parameters[:jungle] == '1' || parameters[:mid] == '1' || parameters[:support] == '1' || parameters[:bot] == '1'
      RoleList.transaction do
        RoleList.new.save!
        @rl_id = RoleList.maximum(:id)
      end

      role_list = []
      if parameters[:top] == '1'
        role_list.push(ConcreteRole.find_by(name: "Top").id)
      end
      if parameters[:jungle] == '1'
        role_list.push(ConcreteRole.find_by(name: "Jungle").id)
      end
      if parameters[:mid] == '1'
        role_list.push(ConcreteRole.find_by(name: "Mid").id)
      end
      if parameters[:support] == '1'
        role_list.push(ConcreteRole.find_by(name: "Support").id)
      end
      if parameters[:bot] == '1'
        role_list.push(ConcreteRole.find_by(name: "Bot").id)
      end

      ActiveRecord::Base.transaction do
        role_list.each do |role|
          ActiveRecord::Base.connection.execute("INSERT INTO roles (role_list_id, concrete_role_id) VALUES (#{@rl_id},#{role});")
        end

        ActiveRecord::Base.connection.execute("INSERT INTO player_is_lfgs (player_id, role_list_id, concrete_league_id_min, concrete_league_id_max) VALUES (#{current_player.id},#{@rl_id},#{ActiveRecord::Base.sanitize parameters[:minimal_league_id]},#{ActiveRecord::Base.sanitize parameters[:maximal_league_id]});")
      end
      redirect_to current_player
    else
      @error = 1
      render 'new'
    end
  end

  def destroy
    if current_player.nil?
      redirect_to login_path
      return
    end

    poster_id = params[:format]
    poster = PlayerIsLfg.find_by(id: poster_id)
    if poster.nil?
      redirect_to current_player
      return
    end
    if current_player.id.to_i == poster.player_id.to_i
      role_list_id = poster.role_list_id
      ActiveRecord::Base.connection.execute(
        "BEGIN TRANSACTION;
        DELETE FROM role_lists WHERE id=#{role_list_id};
        DELETE FROM roles WHERE role_list_id=#{role_list_id};
        DELETE FROM player_is_lfgs WHERE id=#{poster_id};
        END TRANSACTION;"
      )
    end
    redirect_to current_player
  end

  private

    def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

end
