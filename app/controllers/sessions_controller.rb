class SessionsController < ApplicationController
  def new
  end

  def create
    #player = Player.find_by(login: params[:session][:login])
    player_login =  ActiveRecord::Base.sanitize params[:session][:login].downcase
    player = Player.find_by_sql("SELECT * FROM players WHERE  lower(players.login) LIKE #{player_login} LIMIT 1")[0]
    #raise player.inspect
    if player && player.authenticate(params[:session][:password])
      log_in player
      params[:session][:remember_me] == '1' ? remember(player) : forget(player)
      #remember player
      #redirect_to player
      redirect_back_or player
    else
      #flash.now[:danger] = 'Invalid email/password combination'
      @error = 1
      render 'new'
    end
  end

  def destroy
    log_out
    redirect_to root_path
  end
end
