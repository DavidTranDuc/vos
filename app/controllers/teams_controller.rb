class TeamsController < ApplicationController

  def new
    if(current_player.nil?)
      redirect_to login_path
    end
  end

  def create
    #raise params[:new_team].inspect

    if(current_player.nil?)
      redirect_to login_path
      return
    end

    @team = Team.new
    parameters = params[:new_team]

    @error = 0
    if parameters[:concrete_league_id_min_lfm].to_i > parameters[:concrete_league_id_max_lfm].to_i
      @error = 2
      render 'new'
      return
    else
      RoleList.transaction do
        RoleList.new.save!
        @rl_id = RoleList.maximum(:id)
      end

      @team.name = parameters[:name]
      @team.note = parameters[:note]
      @team.id_event = parameters[:event_id]
      @team.id_player = current_player.id
      @team.id_region = current_player.region_id
      @team.id_role_list = @rl_id
      @team.concrete_league_id_min_lfm = parameters[:concrete_league_id_min_lfm]
      @team.concrete_league_id_max_lfm = parameters[:concrete_league_id_max_lfm]
      @team.concrete_league_id_min_plays_in = current_player.concrete_league_id
      @team.concrete_league_id_max_plays_in = current_player.concrete_league_id
      @team.created_at = Time.now
      @team.updated_at = Time.now

      if @team.valid?
        ActiveRecord::Base.transaction do
          ActiveRecord::Base.connection.execute("INSERT INTO roles (role_list_id, concrete_role_id) VALUES (#{@rl_id},#{parameters[:captain_concrete_role_id]});")
          @team.save
          #create player membership
          team_id = Team.maximum(:id)
          membership = PlayerTeam.new
          membership.concrete_role_id = parameters[:captain_concrete_role_id]
          membership.player_id = current_player.id
          membership.team_id = team_id
          membership.created_at = Time.now
          membership.updated_at = Time.now
          membership.save
        end
      else
        render 'new'
        return
      end
      redirect_to current_player
    end
  end

  def team_invites_player
    #raise params[:invite_data].inspect
    if params[:invite_data].nil?
      redirect_to players_path
      return
    end

    parameters = params[:invite_data]
    player_lfg = PlayerIsLfg.find_by(id: parameters[:player_lfg_id])
    to_be_invited_player = Player.find_by(id: player_lfg.player_id)
    player_roles = get_player_lfgs_roles(parameters[:player_lfg_id])
    players_of_team = get_team_members(parameters[:team_id])
    team_roles = get_team_occupied_roles(parameters[:team_id])
    team = Team.find_by(id: parameters[:team_id])

    @isValidR = 0
    #Check if to be invited player is able to play assigned role
    player_roles.each do |role|
      if role.id.to_i == parameters[:concrete_role_id].to_i
        @isValidR = 1
        break
      end
    end

    @isValidM = 1
    #Check if to be invited player is not already member of team
    players_of_team.each do |pot|
      if pot.id.to_i == to_be_invited_player.id.to_i
        @isValidM = 0
        break
      end
    end

    @isValidTR = 1
    #Check if role that to be invited player wants to play isn't already occupied in team roster
    team_roles.each do |tr|
      if tr.id.to_i == parameters[:concrete_role_id].to_i
        @isValidTR = 0
      end
    end

    #Check if team meets player's league range
    #@isValidL
    #if team.concrete_league_id_max_plays_in.to_i >= player_lfg.concrete_league_id_min || team.concrete_league_id_min_plays_in.to_i >= player_lfg.concrete_league_id_max

    #end

    #Check description length
    @isValidD = 0
    if parameters[:note].to_s.length < 530
      @isValidD = 1
    end

    #Check if player is from same region as is team
    @isValidRe = 0
    if to_be_invited_player.region_id.to_i == team.id_region.to_i
      @isValidRe = 1
    end

    #raise "#{@isValidR}  #{@isValidM}  #{@isValidTR}  #{@isValidD}"

    if (@isValidR == 1 && @isValidM == 1 && @isValidTR == 1 && @isValidD == 1 && @isValidRe == 1)
      @invitation = TeamInvitedPlayer.new
      @invitation.team_id = parameters[:team_id]
      @invitation.player_is_lfg_id = parameters[:player_lfg_id]
      @invitation.concrete_role_id = parameters[:concrete_role_id]
      @invitation.note = parameters[:note]
      @invitation.created_at = Time.now
      @invitation.updated_at = Time.now

      @invitation.save
    end

  end

  def index
    @poster_teams = Team.find_by_sql("SELECT * FROM teams")
  end

  def show
    @team = Team.find_by_sql("SELECT * FROM teams WHERE id=#{params[:id]} LIMIT 1")[0]
  end
end
