class PlayerIsLfg < ActiveRecord::Base
  has_many :concrete_league
  has_many :concrete_league
  has_many :player
  has_many :event
  has_many :role_list
  belongs_to :team_invited_player
end
