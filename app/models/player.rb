class Player < ActiveRecord::Base
  has_many :concrete_league
  has_many :region
  belongs_to :player_requested_membership_in_team
  belongs_to :player_team
  belongs_to :team
  belongs_to :player_is_lfg

  attr_accessor :remember_token

  validates :login, presence: true,
                    uniqueness: { case_sensitive: false },
                    length: { maximum: 50 }
  has_secure_password
  validates :password, presence: true, length: { minimum: 6 }
  validates :name, presence: true

  validates :name, uniqueness: { scope: :region_id }


  def Player.digest(string)
    cost = ActiveModel::SecurePassword.min_cost ? BCrypt::Engine::MIN_COST :
        BCrypt::Engine.cost
    BCrypt::Password.create(string, cost: cost)
  end

  def Player.new_token
    SecureRandom.urlsafe_base64
  end

  def remember
    self.remember_token = Player.new_token
    update_attribute(:remember_digest, Player.digest(remember_token))
  end

  def authenticated?(remember_token)
    if remember_digest.nil?
      false
    else
      BCrypt::Password.new(remember_digest).is_password?(remember_token)
    end

  end

  def forget
    update_attribute(:remember_digest, nil)
  end
end
