class PlayerRequestedMembershipInTeam < ActiveRecord::Base
  has_many :player
  has_many :team
  has_many :role_list
end
