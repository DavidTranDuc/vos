class RoleList < ActiveRecord::Base
  belongs_to :player_is_lfg
  belongs_to :player_team
  belongs_to :player_requested_membership_in_team
  belongs_to :team
  belongs_to :role
end
