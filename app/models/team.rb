class Team < ActiveRecord::Base
  has_many :event
  has_many :player
  has_many :region
  has_many :role_list
  has_many :concrete_league
  has_many :concrete_league
  has_many :concrete_league
  has_many :concrete_league
  belongs_to :player_team
  belongs_to :team_invited_player
  belongs_to :player_requested_membership_in_team

  validates :id_event, presence: true
  validates :id_player, presence: true
  validates :id_region, presence: true
  validates :id_role_list, presence: true

  validates :concrete_league_id_max_lfm, presence: true
  validates :concrete_league_id_max_plays_in, presence: true
  validates :concrete_league_id_min_lfm, presence: true
  validates :concrete_league_id_min_plays_in, presence: true

  validates :name, uniqueness: { case_sensitive: false },
                   presence: true,
                   length: { maximum: 50 }

  validates :note, length: { maximum: 500 }

  validates :created_at, presence: true
  validates :created_at, presence: true
end
