class PlayerTeam < ActiveRecord::Base
  has_many :player
  has_many :team
  has_many :concrete_roles
end
