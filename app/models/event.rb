class Event < ActiveRecord::Base
  has_many :host
  has_many :region
  belongs_to :team
  belongs_to :player_is_lfg
end
