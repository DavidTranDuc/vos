$ = function(id) {
    return document.getElementById(id);
}

var show = function(id) {
    $(id).style.display ='block';
}
var hide = function(id) {
    $(id).style.display ='none';
}

function get_roles(player_lfg_id) {
    var request = new XMLHttpRequest();

    request.responseType = "text";
    request.onreadystatechange = function(){
        if(request.readyState == 4 && request.status == 200){
            $('invite_data_player_lfg_id').value = player_lfg_id;
            var prem = $('invite_data_concrete_role_id');
            prem.innerHTML = request.responseText;
            show('invite-player');
        }
    };

    request.open("GET", "/players/get_roles?player_lfg_id=" + player_lfg_id, true);
    request.send();
}