--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.2
-- Dumped by pg_dump version 9.5.2

-- Started on 2016-05-01 21:26:49

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 12355)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2257 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 193 (class 1259 OID 17878)
-- Name: concrete_leagues; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE concrete_leagues (
    id integer NOT NULL,
    division character varying,
    rating integer,
    img character varying
);


ALTER TABLE concrete_leagues OWNER TO "VOS";

--
-- TOC entry 192 (class 1259 OID 17876)
-- Name: concrete_leagues_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE concrete_leagues_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE concrete_leagues_id_seq OWNER TO "VOS";

--
-- TOC entry 2258 (class 0 OID 0)
-- Dependencies: 192
-- Name: concrete_leagues_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE concrete_leagues_id_seq OWNED BY concrete_leagues.id;


--
-- TOC entry 199 (class 1259 OID 17908)
-- Name: concrete_roles; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE concrete_roles (
    id integer NOT NULL,
    name character varying
);


ALTER TABLE concrete_roles OWNER TO "VOS";

--
-- TOC entry 198 (class 1259 OID 17906)
-- Name: concrete_roles_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE concrete_roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE concrete_roles_id_seq OWNER TO "VOS";

--
-- TOC entry 2259 (class 0 OID 0)
-- Dependencies: 198
-- Name: concrete_roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE concrete_roles_id_seq OWNED BY concrete_roles.id;


--
-- TOC entry 207 (class 1259 OID 17952)
-- Name: events; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE events (
    id integer NOT NULL,
    host_id integer,
    region_id integer,
    name character varying,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE events OWNER TO "VOS";

--
-- TOC entry 206 (class 1259 OID 17950)
-- Name: events_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE events_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE events_id_seq OWNER TO "VOS";

--
-- TOC entry 2260 (class 0 OID 0)
-- Dependencies: 206
-- Name: events_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE events_id_seq OWNED BY events.id;


--
-- TOC entry 205 (class 1259 OID 17941)
-- Name: hosts; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE hosts (
    id integer NOT NULL,
    name character varying,
    login character varying,
    password_digest character varying,
    email character varying,
    activation_digest character varying,
    activated boolean,
    activated_at timestamp without time zone,
    note text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE hosts OWNER TO "VOS";

--
-- TOC entry 204 (class 1259 OID 17939)
-- Name: hosts_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE hosts_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE hosts_id_seq OWNER TO "VOS";

--
-- TOC entry 2261 (class 0 OID 0)
-- Dependencies: 204
-- Name: hosts_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE hosts_id_seq OWNED BY hosts.id;


--
-- TOC entry 201 (class 1259 OID 17919)
-- Name: player_is_lfgs; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE player_is_lfgs (
    id integer NOT NULL,
    player_id integer,
    role_list_id integer,
    concrete_league_id_min integer,
    concrete_league_id_max integer,
    event_id integer,
    note text
);


ALTER TABLE player_is_lfgs OWNER TO "VOS";

--
-- TOC entry 200 (class 1259 OID 17917)
-- Name: player_is_lfgs_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE player_is_lfgs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE player_is_lfgs_id_seq OWNER TO "VOS";

--
-- TOC entry 2262 (class 0 OID 0)
-- Dependencies: 200
-- Name: player_is_lfgs_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE player_is_lfgs_id_seq OWNED BY player_is_lfgs.id;


--
-- TOC entry 191 (class 1259 OID 17867)
-- Name: player_requested_membership_in_teams; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE player_requested_membership_in_teams (
    id integer NOT NULL,
    player_id integer,
    team_id integer,
    role_list_id integer,
    note text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE player_requested_membership_in_teams OWNER TO "VOS";

--
-- TOC entry 190 (class 1259 OID 17865)
-- Name: player_requested_membership_in_teams_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE player_requested_membership_in_teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE player_requested_membership_in_teams_id_seq OWNER TO "VOS";

--
-- TOC entry 2263 (class 0 OID 0)
-- Dependencies: 190
-- Name: player_requested_membership_in_teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE player_requested_membership_in_teams_id_seq OWNED BY player_requested_membership_in_teams.id;


--
-- TOC entry 189 (class 1259 OID 17859)
-- Name: player_teams; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE player_teams (
    id integer NOT NULL,
    player_id integer,
    team_id integer,
    role_list_id integer,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE player_teams OWNER TO "VOS";

--
-- TOC entry 188 (class 1259 OID 17857)
-- Name: player_teams_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE player_teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE player_teams_id_seq OWNER TO "VOS";

--
-- TOC entry 2264 (class 0 OID 0)
-- Dependencies: 188
-- Name: player_teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE player_teams_id_seq OWNED BY player_teams.id;


--
-- TOC entry 183 (class 1259 OID 17826)
-- Name: players; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE players (
    id integer NOT NULL,
    region_id integer,
    concrete_league_id integer,
    name character varying,
    login character varying,
    win integer,
    lose integer,
    password_digest character varying,
    remember_digest character varying,
    activation_digest character varying,
    activated boolean,
    activated_at timestamp without time zone,
    note text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE players OWNER TO "VOS";

--
-- TOC entry 182 (class 1259 OID 17824)
-- Name: players_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE players_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE players_id_seq OWNER TO "VOS";

--
-- TOC entry 2265 (class 0 OID 0)
-- Dependencies: 182
-- Name: players_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE players_id_seq OWNED BY players.id;


--
-- TOC entry 187 (class 1259 OID 17848)
-- Name: regions; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE regions (
    id integer NOT NULL,
    name character varying,
    short character varying
);


ALTER TABLE regions OWNER TO "VOS";

--
-- TOC entry 186 (class 1259 OID 17846)
-- Name: regions_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE regions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE regions_id_seq OWNER TO "VOS";

--
-- TOC entry 2266 (class 0 OID 0)
-- Dependencies: 186
-- Name: regions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE regions_id_seq OWNED BY regions.id;


--
-- TOC entry 195 (class 1259 OID 17889)
-- Name: role_lists; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE role_lists (
    id integer NOT NULL
);


ALTER TABLE role_lists OWNER TO "VOS";

--
-- TOC entry 194 (class 1259 OID 17887)
-- Name: role_lists_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE role_lists_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE role_lists_id_seq OWNER TO "VOS";

--
-- TOC entry 2267 (class 0 OID 0)
-- Dependencies: 194
-- Name: role_lists_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE role_lists_id_seq OWNED BY role_lists.id;


--
-- TOC entry 197 (class 1259 OID 17897)
-- Name: roles; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE roles (
    id integer NOT NULL,
    role_list_id integer,
    concrete_role_id integer,
    name character varying
);


ALTER TABLE roles OWNER TO "VOS";

--
-- TOC entry 196 (class 1259 OID 17895)
-- Name: roles_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE roles_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE roles_id_seq OWNER TO "VOS";

--
-- TOC entry 2268 (class 0 OID 0)
-- Dependencies: 196
-- Name: roles_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE roles_id_seq OWNED BY roles.id;


--
-- TOC entry 181 (class 1259 OID 17817)
-- Name: schema_migrations; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE schema_migrations (
    version character varying NOT NULL
);


ALTER TABLE schema_migrations OWNER TO "VOS";

--
-- TOC entry 203 (class 1259 OID 17930)
-- Name: team_invited_players; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE team_invited_players (
    id integer NOT NULL,
    team_id integer,
    player_is_lfg_id integer,
    note text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE team_invited_players OWNER TO "VOS";

--
-- TOC entry 202 (class 1259 OID 17928)
-- Name: team_invited_players_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE team_invited_players_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE team_invited_players_id_seq OWNER TO "VOS";

--
-- TOC entry 2269 (class 0 OID 0)
-- Dependencies: 202
-- Name: team_invited_players_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE team_invited_players_id_seq OWNED BY team_invited_players.id;


--
-- TOC entry 185 (class 1259 OID 17837)
-- Name: teams; Type: TABLE; Schema: public; Owner: VOS
--

CREATE TABLE teams (
    id integer NOT NULL,
    id_event integer,
    id_player integer,
    id_region integer,
    id_role_list integer,
    concrete_league_id_min_plays_in integer,
    concrete_league_id_max_plays_in integer,
    concrete_league_id_min_lfm integer,
    concrete_league_id_max_lfm character varying,
    name character varying,
    note text,
    created_at timestamp without time zone NOT NULL,
    updated_at timestamp without time zone NOT NULL
);


ALTER TABLE teams OWNER TO "VOS";

--
-- TOC entry 184 (class 1259 OID 17835)
-- Name: teams_id_seq; Type: SEQUENCE; Schema: public; Owner: VOS
--

CREATE SEQUENCE teams_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE teams_id_seq OWNER TO "VOS";

--
-- TOC entry 2270 (class 0 OID 0)
-- Dependencies: 184
-- Name: teams_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: VOS
--

ALTER SEQUENCE teams_id_seq OWNED BY teams.id;


--
-- TOC entry 2074 (class 2604 OID 17881)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY concrete_leagues ALTER COLUMN id SET DEFAULT nextval('concrete_leagues_id_seq'::regclass);


--
-- TOC entry 2077 (class 2604 OID 17911)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY concrete_roles ALTER COLUMN id SET DEFAULT nextval('concrete_roles_id_seq'::regclass);


--
-- TOC entry 2081 (class 2604 OID 17955)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY events ALTER COLUMN id SET DEFAULT nextval('events_id_seq'::regclass);


--
-- TOC entry 2080 (class 2604 OID 17944)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY hosts ALTER COLUMN id SET DEFAULT nextval('hosts_id_seq'::regclass);


--
-- TOC entry 2078 (class 2604 OID 17922)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY player_is_lfgs ALTER COLUMN id SET DEFAULT nextval('player_is_lfgs_id_seq'::regclass);


--
-- TOC entry 2073 (class 2604 OID 17870)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY player_requested_membership_in_teams ALTER COLUMN id SET DEFAULT nextval('player_requested_membership_in_teams_id_seq'::regclass);


--
-- TOC entry 2072 (class 2604 OID 17862)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY player_teams ALTER COLUMN id SET DEFAULT nextval('player_teams_id_seq'::regclass);


--
-- TOC entry 2069 (class 2604 OID 17829)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY players ALTER COLUMN id SET DEFAULT nextval('players_id_seq'::regclass);


--
-- TOC entry 2071 (class 2604 OID 17851)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY regions ALTER COLUMN id SET DEFAULT nextval('regions_id_seq'::regclass);


--
-- TOC entry 2075 (class 2604 OID 17892)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY role_lists ALTER COLUMN id SET DEFAULT nextval('role_lists_id_seq'::regclass);


--
-- TOC entry 2076 (class 2604 OID 17900)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY roles ALTER COLUMN id SET DEFAULT nextval('roles_id_seq'::regclass);


--
-- TOC entry 2079 (class 2604 OID 17933)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY team_invited_players ALTER COLUMN id SET DEFAULT nextval('team_invited_players_id_seq'::regclass);


--
-- TOC entry 2070 (class 2604 OID 17840)
-- Name: id; Type: DEFAULT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY teams ALTER COLUMN id SET DEFAULT nextval('teams_id_seq'::regclass);


--
-- TOC entry 2235 (class 0 OID 17878)
-- Dependencies: 193
-- Data for Name: concrete_leagues; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY concrete_leagues (id, division, rating, img) FROM stdin;
1	Unranked	0	unranked
2	Bronze V	100	bronze_5
3	Bronze IV	110	bronze_4
4	Bronze III	120	bronze_3
5	Bronze II	130	bronze_2
6	Bronze I	140	bronze_1
7	Silver V	200	silver_5
8	Silver IV	210	silver_4
9	Silver III	220	silver_3
10	Silver II	230	silver_2
11	Silver I	240	silver_1
12	Gold V	300	gold_5
13	Gold IV	310	gold_4
14	Gold III	320	gold_3
15	Gold II	330	gold_2
16	Gold I	340	gold_1
17	Platinum V	400	platinum_5
18	Platinum IV	410	platinum_4
19	Platinum III	420	platinum_3
20	Platinum II	430	platinum_2
21	Platinum I	440	platinum_1
22	Diamond V	500	diamond_5
23	Diamond IV	510	diamond_4
24	Diamond III	520	diamond_3
25	Diamond II	530	diamond_2
26	Diamond I	540	diamond_1
27	Master	640	master_1
28	Challenger	740	challenger_1
\.


--
-- TOC entry 2271 (class 0 OID 0)
-- Dependencies: 192
-- Name: concrete_leagues_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('concrete_leagues_id_seq', 28, true);


--
-- TOC entry 2241 (class 0 OID 17908)
-- Dependencies: 199
-- Data for Name: concrete_roles; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY concrete_roles (id, name) FROM stdin;
1	Top
2	Mid
3	Jungle
4	Support
5	Bot
\.


--
-- TOC entry 2272 (class 0 OID 0)
-- Dependencies: 198
-- Name: concrete_roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('concrete_roles_id_seq', 5, true);


--
-- TOC entry 2249 (class 0 OID 17952)
-- Dependencies: 207
-- Data for Name: events; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY events (id, host_id, region_id, name, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2273 (class 0 OID 0)
-- Dependencies: 206
-- Name: events_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('events_id_seq', 1, false);


--
-- TOC entry 2247 (class 0 OID 17941)
-- Dependencies: 205
-- Data for Name: hosts; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY hosts (id, name, login, password_digest, email, activation_digest, activated, activated_at, note, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2274 (class 0 OID 0)
-- Dependencies: 204
-- Name: hosts_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('hosts_id_seq', 1, false);


--
-- TOC entry 2243 (class 0 OID 17919)
-- Dependencies: 201
-- Data for Name: player_is_lfgs; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY player_is_lfgs (id, player_id, role_list_id, concrete_league_id_min, concrete_league_id_max, event_id, note) FROM stdin;
3	11	11	6	11	\N	\N
7	12	15	7	16	\N	\N
9	23	17	16	19	\N	\N
12	13	20	26	28	\N	\N
13	13	21	22	27	\N	\N
14	14	22	2	4	\N	\N
15	15	23	12	16	\N	\N
16	16	24	25	27	\N	\N
17	17	25	4	9	\N	\N
18	18	26	9	14	\N	\N
19	19	27	23	25	\N	\N
20	19	28	21	22	\N	\N
21	20	29	12	14	\N	\N
\.


--
-- TOC entry 2275 (class 0 OID 0)
-- Dependencies: 200
-- Name: player_is_lfgs_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('player_is_lfgs_id_seq', 21, true);


--
-- TOC entry 2233 (class 0 OID 17867)
-- Dependencies: 191
-- Data for Name: player_requested_membership_in_teams; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY player_requested_membership_in_teams (id, player_id, team_id, role_list_id, note, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2276 (class 0 OID 0)
-- Dependencies: 190
-- Name: player_requested_membership_in_teams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('player_requested_membership_in_teams_id_seq', 1, false);


--
-- TOC entry 2231 (class 0 OID 17859)
-- Dependencies: 189
-- Data for Name: player_teams; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY player_teams (id, player_id, team_id, role_list_id, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2277 (class 0 OID 0)
-- Dependencies: 188
-- Name: player_teams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('player_teams_id_seq', 1, false);


--
-- TOC entry 2225 (class 0 OID 17826)
-- Dependencies: 183
-- Data for Name: players; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY players (id, region_id, concrete_league_id, name, login, win, lose, password_digest, remember_digest, activation_digest, activated, activated_at, note, created_at, updated_at) FROM stdin;
12	5	15	PlayerB	playerb	37	24	$2a$10$VcoiHZ1r/fWdwZW3fSzJIOHlbwibtRl8VzZWMI8BVmX2uuLDtE5s.	\N	\N	t	\N	\N	2016-04-09 18:42:31	2016-04-09 18:42:31
13	5	28	PlayerC	playerc	407	352	$2a$10$/btj8eVpFDkpGIkLoJP0OOBx4rCfQErr1GAwVgcy9qN2jdL4Glyr.	\N	\N	t	\N	\N	2016-04-09 18:42:31	2016-04-09 18:42:31
14	5	3	PlayerD	playerd	7	19	$2a$10$JZYjWB5nPk3XdqmaA4ndM.hqmWXTz6PU4ECleFBGr74zBljCmP.LK	\N	\N	t	\N	\N	2016-04-09 18:42:31	2016-04-09 18:42:31
16	5	27	PlayerF	playerf	103	45	$2a$10$sLOMuxMhBJW5SR5xg8nYYukish4azBixV1brMn4U/v5twDvLCS8Pa	\N	\N	t	\N	\N	2016-04-09 18:42:31	2016-04-09 18:42:31
17	5	7	PlayerG	playerg	57	58	$2a$10$P8lkS8AcrED/PWHfByUMDOxWnPbsKdhhGCmrD3VvsFGspce0H/Qoq	\N	\N	t	\N	\N	2016-04-09 18:42:31	2016-04-09 18:42:31
18	5	11	PlayerH	playerh	87	84	$2a$10$IUxRTA52pOSi8ezW8NcxeO53lEljaooa14Um6hJI1OI5nebF5U6we	\N	\N	t	\N	\N	2016-04-09 18:42:31	2016-04-09 18:42:31
19	5	24	PlayerI	playeri	207	153	$2a$10$gEAEg49zCczktd8G9NXn5.r9mD2FCnmNWvIxNN1I7fNiEKaOBeBay	\N	\N	t	\N	\N	2016-04-09 18:42:32	2016-04-09 18:42:32
20	5	14	PlayerJ	playerj	70	78	$2a$10$0KcNehH3ggkaw2Jvg/kNfe1/TTVEOetBKnRntciNtY6Vx2XHTTEXK	\N	\N	t	\N	\N	2016-04-09 18:42:32	2016-04-09 18:42:32
24	5	0	PlayerK	playerk	70	78	$2a$10$qHMEFnJkdFcxDugMgI58tOfwPkoCf/dNJkvm3.gWzk0okB6wbacnu	\N	\N	t	\N	\N	2016-05-01 15:17:25	2016-05-01 15:17:25
25	5	1	PlayerL	playerl	70	78	$2a$10$U5f9ZfEyyRHhV6n39rpD4uKsYoSDwU5eyTRkMPvtGBf1DT3DhtLAm	\N	\N	t	\N	\N	2016-05-01 15:17:25	2016-05-01 15:17:25
11	5	7	PlayerA	playera	33	28	$2a$10$dWst4ijHIq5XCNrxSnZX7ezNo1L93K2dt2xLyO/w.j/EVnSuNrg/e	\N	\N	t	\N	\N	2016-04-09 18:42:31	2016-04-10 10:57:12
26	5	2	PlayerM	playerm	70	78	$2a$10$tZOnQTKiFWgRoU29u8t3kejmbZS0G102jZyQnBtwBCXdov2y2.xUG	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
27	5	3	PlayerN	playern	70	78	$2a$10$hMZ0GLcskpwPOt8N3l5S0O/cwvvSZC0y.OIvBFph5iDv1PieqIQYa	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
28	5	4	PlayerO	playero	70	78	$2a$10$1N3ZdfHOln4HeRvu7A/pu.JjElTN5swychEJ42XFvMh0rkPEUNHU.	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
29	5	5	PlayerP	playerp	70	78	$2a$10$V5trE.dumx3D9cmyaEvemeJQrD8/fG01UCF6T/feGBsDDJBxyi5UC	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
30	5	6	PlayerQ	playerq	70	78	$2a$10$a0yiUXGLtroBgvLS3TF8EeKmOxJL.GmiL.ra.goLrc68.qTEW4dYa	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
31	5	7	PlayerR	playerr	70	78	$2a$10$v/4ssgYh9BF7SlrgJQlisuprKcUIYW/0SyySRwNZAwXNUMqXqtpIm	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
32	5	8	PlayerS	players	70	78	$2a$10$dFYH62GMwxhKAC/CUHF8zeiZDpZrfSbcc9t/UF0Ay4obVpd0YKnZ6	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
33	5	9	PlayerT	playert	70	78	$2a$10$K9uvz6iXzeSuX0Dg8FXUV.X7Qyzdc00anoJujAlEx9qrDkbglo5uG	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
34	5	10	PlayerU	playeru	70	78	$2a$10$OX2HO3E0qKs9Yjl5dvbM7.uiFR7nE/zYcTNIXofPaSYnlAfLMOEJ6	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
35	5	11	PlayerV	playerv	70	78	$2a$10$hgc0bkfN6Vl696Yo27DqiuhQifib0A3kDXFIGQaV7zdS7rWz3PWH2	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
36	5	12	PlayerX	playerx	70	78	$2a$10$NA2vs.U0Z7qhVK9oGjCvsOSQDHTBggRxEzMUj./UdYqyisXr0RBz2	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
37	5	13	PlayerY	playery	70	78	$2a$10$ZrgS9vKN07YJWHeJS61io.lArUgURG2RCFV8CW3XToKDkETSvxOb6	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
38	5	14	PlayerZ	playerz	70	78	$2a$10$s63bWmRk6xRMZFhm6eUK7uTFFoeRhkWj4rP2n0xuJN.2iHqA6scfS	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
39	5	15	Player1	player1	70	78	$2a$10$J/DNXvhZBS0j4yOD0dgrP.1v4zz4798jEy5BfpFq47us8h13EKLpW	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
40	5	16	Player2	player2	70	78	$2a$10$agfD7TrttiPuSNd64d6aWe8a7iStosHZWn13bWZBd.rjLNySU5hzK	\N	\N	t	\N	\N	2016-05-01 15:17:26	2016-05-01 15:17:26
41	5	17	Player3	player3	70	78	$2a$10$0o5JWevltKFBQj.Dvt/qy.7riOya5FdzJbLYOnM99VusRQpXEoFgG	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
42	5	18	Player4	player4	70	78	$2a$10$1lIt159pAo29B6qGg7zjBuzJF7HbQTxutmZQK3ePnkgULbalr1GsK	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
43	5	19	Player5	player5	70	78	$2a$10$3wwBr.ABpc4OVYlT9bRvYuhqiOAY2HzN0hlngvlRuHxdX7LJQUFai	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
44	5	20	Player6	player6	70	78	$2a$10$IpDqEole44jXqYIGMRvuU.xUWltrvvOmFdxJZdJDWbzY/XhLUBYTW	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
45	5	21	Player7	player7	70	78	$2a$10$SHQEnvrlTdtYzXYmZ7wjleHZdV4cHGvyWYWQIq.PT0qp7KKy/.bKK	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
46	5	22	Player8	player8	70	78	$2a$10$rnr43Q7IWob2.1OPwGZlY..M1.3E62nA4uQuFATDIBF7w0kVT6nv2	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
47	5	23	Player9	player9	70	78	$2a$10$h4PG6Do2bjCxfObR.2YiDe1Khbg1uDVt.I/hxfcrmV4caH5pum6.C	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
22	5	1	Test	Test	0	0	$2a$10$8NDKSNW0B4FRY2ckfK094eq5mQM25EXuBBU7VQo./7Hi8ZnLULrie	\N	\N	t	\N	\N	2016-04-09 21:39:10	2016-04-09 21:39:10
23	4	18	Random	Random	64	53	$2a$10$0VvKvNo/1N3HDGDNpohYJO0DL346o7zjEdnPDOUm2j5igYkCb7zUm	\N	\N	t	\N	\N	2016-04-10 19:25:30	2016-04-10 19:53:06.970883
15	5	1	PlayerE	playere	3	1	$2a$10$yZ6nkwvx1SJ9bZbybPz2w.R4o6IP6PJGL2UkRlmm/4SKNwxORs5Qi	\N	\N	t	\N	\N	2016-04-09 18:42:31	2016-05-01 16:21:30
48	5	24	Player10	player10	70	78	$2a$10$maRvGyNfQBo3w5zsMSgu2eJL2zbTp34mrgxeuurBNq1ZO6npz5APe	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
49	5	25	Player11	player11	70	78	$2a$10$IoLB6i6v4HbQ./JA8H2au.lmL5lB3SxE4mXPZ0U2AcHKB6SsNOF3e	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
50	5	26	Player12	player12	70	78	$2a$10$Lhsy0e/dGFeZYBjoK.e87uw6OimyuuNt8FHJ5z1FpW5t909fbewBy	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
51	5	27	Player13	player13	70	78	$2a$10$JNGqReZuzj4gqQPl/MC4YeWH284sq7PAKyrB5UnKg8cGlgLTrT5ui	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
52	5	28	Player14	player14	70	78	$2a$10$qEYVzCbteQG1Gynn0lmsu.lMnZCRN14LKTno4UC6qNK6O7O0uWrzS	\N	\N	t	\N	\N	2016-05-01 15:17:27	2016-05-01 15:17:27
53	5	1	Test2	test2	0	0	$2a$10$duZEcXjomFPLhl2JZ46v9.1FTuJOIDLQrOHPWaCcZjeae22YI6Tre	\N	\N	t	\N	\N	2016-05-01 16:27:30	2016-05-01 16:27:30
54	5	1	Test3	test3	0	0	$2a$10$GeXKkRVvH8YNA8c0DSkhy.5Ou2OEXC8DOYJuqv7sNpNh/z.yCZAGa	\N	\N	\N	\N	\N	2016-05-01 16:31:58	2016-05-01 16:31:58
55	5	12	Test4	test4	56	45	$2a$10$aUzfqTaiJFdJOnzOQ5IkdOA2wqrO3OYWpHmCFgDRYT5k0LBYUdpGu	\N	\N	\N	\N	\N	2016-05-01 16:36:10	2016-05-01 16:37:59
\.


--
-- TOC entry 2278 (class 0 OID 0)
-- Dependencies: 182
-- Name: players_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('players_id_seq', 55, true);


--
-- TOC entry 2229 (class 0 OID 17848)
-- Dependencies: 187
-- Data for Name: regions; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY regions (id, name, short) FROM stdin;
1	Korea	kr
2	Japan	jp
3	North America	na
4	Europe West	euw
5	Europe Nordic & East	eune
6	Oceania	oce
7	Brazil	br
8	LAS	las
9	LAN	lan
10	Russia	ru
11	Turkey	tr
\.


--
-- TOC entry 2279 (class 0 OID 0)
-- Dependencies: 186
-- Name: regions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('regions_id_seq', 11, true);


--
-- TOC entry 2237 (class 0 OID 17889)
-- Dependencies: 195
-- Data for Name: role_lists; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY role_lists (id) FROM stdin;
1
2
3
4
5
6
7
8
9
11
15
17
20
21
22
23
24
25
26
27
28
29
\.


--
-- TOC entry 2280 (class 0 OID 0)
-- Dependencies: 194
-- Name: role_lists_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('role_lists_id_seq', 29, true);


--
-- TOC entry 2239 (class 0 OID 17897)
-- Dependencies: 197
-- Data for Name: roles; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY roles (id, role_list_id, concrete_role_id, name) FROM stdin;
4	11	1	\N
5	11	3	\N
6	11	2	\N
7	11	4	\N
8	11	5	\N
12	15	3	\N
13	15	5	\N
15	17	2	\N
16	17	5	\N
19	20	1	\N
20	20	2	\N
21	21	3	\N
22	21	4	\N
23	22	3	\N
24	22	4	\N
25	22	5	\N
26	23	3	\N
27	24	4	\N
28	24	5	\N
29	25	2	\N
30	25	4	\N
31	26	1	\N
32	26	2	\N
33	26	5	\N
34	27	1	\N
35	27	2	\N
36	28	3	\N
37	28	4	\N
38	28	5	\N
39	29	2	\N
40	29	4	\N
\.


--
-- TOC entry 2281 (class 0 OID 0)
-- Dependencies: 196
-- Name: roles_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('roles_id_seq', 40, true);


--
-- TOC entry 2223 (class 0 OID 17817)
-- Dependencies: 181
-- Data for Name: schema_migrations; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY schema_migrations (version) FROM stdin;
20160404185209
20160404185554
20160404185707
20160404185742
20160404185927
20160404190411
20160404190444
20160404190531
20160404190623
20160404190659
20160404190813
20160404190901
20160404190951
\.


--
-- TOC entry 2245 (class 0 OID 17930)
-- Dependencies: 203
-- Data for Name: team_invited_players; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY team_invited_players (id, team_id, player_is_lfg_id, note, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2282 (class 0 OID 0)
-- Dependencies: 202
-- Name: team_invited_players_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('team_invited_players_id_seq', 1, false);


--
-- TOC entry 2227 (class 0 OID 17837)
-- Dependencies: 185
-- Data for Name: teams; Type: TABLE DATA; Schema: public; Owner: VOS
--

COPY teams (id, id_event, id_player, id_region, id_role_list, concrete_league_id_min_plays_in, concrete_league_id_max_plays_in, concrete_league_id_min_lfm, concrete_league_id_max_lfm, name, note, created_at, updated_at) FROM stdin;
\.


--
-- TOC entry 2283 (class 0 OID 0)
-- Dependencies: 184
-- Name: teams_id_seq; Type: SEQUENCE SET; Schema: public; Owner: VOS
--

SELECT pg_catalog.setval('teams_id_seq', 1, false);


--
-- TOC entry 2094 (class 2606 OID 17886)
-- Name: concrete_leagues_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY concrete_leagues
    ADD CONSTRAINT concrete_leagues_pkey PRIMARY KEY (id);


--
-- TOC entry 2100 (class 2606 OID 17916)
-- Name: concrete_roles_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY concrete_roles
    ADD CONSTRAINT concrete_roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2108 (class 2606 OID 17960)
-- Name: events_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY events
    ADD CONSTRAINT events_pkey PRIMARY KEY (id);


--
-- TOC entry 2106 (class 2606 OID 17949)
-- Name: hosts_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY hosts
    ADD CONSTRAINT hosts_pkey PRIMARY KEY (id);


--
-- TOC entry 2102 (class 2606 OID 17927)
-- Name: player_is_lfgs_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY player_is_lfgs
    ADD CONSTRAINT player_is_lfgs_pkey PRIMARY KEY (id);


--
-- TOC entry 2092 (class 2606 OID 17875)
-- Name: player_requested_membership_in_teams_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY player_requested_membership_in_teams
    ADD CONSTRAINT player_requested_membership_in_teams_pkey PRIMARY KEY (id);


--
-- TOC entry 2090 (class 2606 OID 17864)
-- Name: player_teams_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY player_teams
    ADD CONSTRAINT player_teams_pkey PRIMARY KEY (id);


--
-- TOC entry 2084 (class 2606 OID 17834)
-- Name: players_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY players
    ADD CONSTRAINT players_pkey PRIMARY KEY (id);


--
-- TOC entry 2088 (class 2606 OID 17856)
-- Name: regions_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY regions
    ADD CONSTRAINT regions_pkey PRIMARY KEY (id);


--
-- TOC entry 2096 (class 2606 OID 17894)
-- Name: role_lists_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY role_lists
    ADD CONSTRAINT role_lists_pkey PRIMARY KEY (id);


--
-- TOC entry 2098 (class 2606 OID 17905)
-- Name: roles_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY roles
    ADD CONSTRAINT roles_pkey PRIMARY KEY (id);


--
-- TOC entry 2104 (class 2606 OID 17938)
-- Name: team_invited_players_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY team_invited_players
    ADD CONSTRAINT team_invited_players_pkey PRIMARY KEY (id);


--
-- TOC entry 2086 (class 2606 OID 17845)
-- Name: teams_pkey; Type: CONSTRAINT; Schema: public; Owner: VOS
--

ALTER TABLE ONLY teams
    ADD CONSTRAINT teams_pkey PRIMARY KEY (id);


--
-- TOC entry 2082 (class 1259 OID 17823)
-- Name: unique_schema_migrations; Type: INDEX; Schema: public; Owner: VOS
--

CREATE UNIQUE INDEX unique_schema_migrations ON schema_migrations USING btree (version);


--
-- TOC entry 2256 (class 0 OID 0)
-- Dependencies: 6
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


-- Completed on 2016-05-01 21:26:49

--
-- PostgreSQL database dump complete
--

