Rails.application.routes.draw do
  root 'static_pages#home'

  post 'players' => 'players#index'

  get 'register'    => 'players#new'
  post 'register' => 'players#create'

  get 'change_password' => 'players#edit'
  post 'change_password' => 'players#update'

  #get 'change_stats' => 'players#edit2'      #disabled due to riot API implementation, manual changes are no longer required
  #patch 'change_stats' => 'players#update2'  #disabled due to riot API implementation, manual changes are no longer required

  #get 'players/:id/change_stats' => 'players#edit2'
  #post 'players/:id/players/:id/change_stats' => 'players#update2'

  put 'api_update' => 'players#put'

  get    'login'   => 'sessions#new'
  post   'login'   => 'sessions#create'
  delete 'logout'  => 'sessions#destroy'

  get 'new_lfg' => 'player_is_lfgs#new'
  post 'new_lfg' => 'player_is_lfgs#create'
  delete "delete_lfg/#{:id}" => 'player_is_lfgs#destroy'

  get 'statistics' => 'static_pages#statistics'

  get 'newteam' => 'teams#new'
  post 'newteam' => 'teams#create'

  post 'team_invites_player' => 'teams#team_invites_player'

  get '/players/get_roles' => 'players#get_roles'

  get 'teams' => 'teams#index'
  post 'teams' => 'teams#index'

  get 'invitations' => 'invites#show'

  post 'invite_accept' => 'invites#accept'
  delete "delete_invitation/#{:id}" => 'invites#destroy'

  resources :players
  resources :player_is_lfgs
  resources :teams
  resources :invites

end
